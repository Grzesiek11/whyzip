# whyzip

The best/worst file compression utility.

## whyzip?

I once wanted to compress a file containing exactly one bilion `O`s with `gzip`. And I was disappointed by the results, oh so disappointed...

```
$ gzip -k alotofo
$ du -h alotofo.gz
948K    alotofo.gz
```

948 KiB for one bilion of THE SAME BYTE repeating over and OVER?! Unbelivable! RIDICULOUS!!! You don't need more than a few bytes to store that information!

## That's whyzip!

This is the solution - `whyzip`, a utility specifically made to compress the most important files - ones with long sequences of the same byte repeating!

## The utility

### Getting whyzip

To start using `whyzip`, simply download the build for your OS from [the releases page](https://gitlab.com/Grzesiek11/whyzip/-/releases).

You can also move the binary (and symlink, on supported platforms) to somewhere in your `PATH` for convenience.

### Using whyzip

The overall syntax is:

```
whyzip [-dk] [file...]
```

Now, to compress `a_file.txt`:

```
whyzip a_file.txt
```

By default `whyzip` will delete the original file after compressing it. To keep it, add `-k` or `--keep`:

```
whyzip -k a_file.txt
```

To decompress `a_file.txt.why`, add `-d` or `--decompress`:

```
whyzip -d a_file.txt.why
```

Alternatively, you can use the `whyunzip` symlink, if it's supported on your platform:

```
whyunzip a_file.txt.why
```

`whyzip` can compress or decompress multiple files at once:

```
whyzip a_file.txt another_file.huh alotofo
```

It can also compress or decompress from `stdin` to `stdout` if no files are provided:

```
whyzip < file > file.wz
whyunzip < file.wz > file
```

It can be used with `tar` to create and unpack a compressed archive:

```
tar c directory | whyzip > archive.tar.why
whyzip < archive.tar.why | tar x
```

### Limitations

This implementation can compress a sequence of at most $`2^{128}`$, or approximately $`3.402823669 \cdot 10^{38}`$ bytes. It's still a really big number, but the format allows for theoretically infinite sequences (by using multiple sequences). This will be improved in a later release.

## The format

The `whyzip` format is very simple:

```
$ xxd -g 1 example.why
00000000: 77 68 79 61 02 02 df 0a 00                       whya.....
```

| Byte | Example    | Size (bytes) | Field description                                  |
| :--: | :--------: | :----------: | :------------------------------------------------: |
| 0-2  | `77 68 79` | 3            | The whyzip magic                                   |
| 3    | `61`       | 1            | Beggining of the 1st sequence. The sequence byte.  |
| 4    | `02`       | 1            | Size of the following field.                       |
| 5-6  | `02 df`    | 0+           | Size-terminated repeat count of the sequence byte. |
| 7    | `0a`       | 1            | Beggining of the 2nd sequence.                     |
| ...  | ...        | ...          | etc.                                               |

**Notes:**

- The repeat count starts at 0 and not 1.
- Due to the above, a non-repeating null byte can be represented as `00 00`.
- The maximum theoretical amount of bytes that can be held by a single sequence is $`2^{8\cdot255}`$, approximately $`1.26238305 \cdot 10^{614}`$. It's a HUGE number.
- You can use multiple in-format sequences for one actual sequence. So it can be theoretically *infinite*.

The recommended suffix for whyzip files is `.why`. The recommended media type is `application/x.whyzip`.

## whyzip vs gzip and xz

In both tests, I use a file with $`10^{9}`$ `O` bytes.

### Performance

```
$ time gzip -k alotofo
gzip -k alotofo  4,93s user 0,12s system 99% cpu 5,058 total
$ time gunzip < alotofo.gz > /dev/null
gunzip < alotofo.gz > /dev/null  3,95s user 0,00s system 99% cpu 3,957 total
$ time xz -k alotofo
xz -k alotofo  14,80s user 0,19s system 99% cpu 15,000 total
$ time unxz < alotofo.xz > /dev/null
unxz < alotofo.xz > /dev/null  2,66s user 0,03s system 99% cpu 2,693 total
$ time whyzip -k alotofo
whyzip -k alotofo  7,00s user 0,32s system 91% cpu 7,972 total
$ time whyunzip < alotofo.why > /dev/null
whyunzip < alotofo.why > /dev/null  2,79s user 0,01s system 99% cpu 2,803 total
```

`gzip` is a bit faster than `whyzip` (I don't know why and how, actually). `xz` is pretty slow, but that's to be expected.

### Compression

```
$ du -h --apparent-size alotofo alotofo.gz alotofo.xz alotofo.why
954M    alotofo
948K    alotofo.gz
143K    alotofo.xz
9       alotofo.why
$ du -b alotofo alotofo.gz alotofo.xz alotofo.why
1000000000      alotofo
970510  alotofo.gz
145584  alotofo.xz
9       alotofo.why
```

`xz` (143 KiB) compresses the file much better than `gzip` (948 KiB), but `whyzip` is just 9 bytes!

## Possible use cases

- Files with a lot of `O`s.
- Largely empty disk images.
- `/dev/zero`.
- The [Whitespace](https://en.wikipedia.org/wiki/Whitespace_(programming_language)) language source files.
- The posibilities are ENDLESS!
- I also wrote about more use cases [on my website](https://grzesiek11.stary.pc.pl/whyzip-use-cases).

## License

`whyzip` is public domain. See [LICENSE](LICENSE).
