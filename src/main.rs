use std::{io, env, fs::{self, File}};

enum Action {
    Compress,
    Decompress,
}

impl Action {
    fn execute_on<I: io::Read, O: io::Write>(&self, istream: &mut I, ostream: &mut O) -> Result<(), whyzip::Error> {
        match &self {
            Action::Compress => whyzip::compress(istream, ostream),
            Action::Decompress => whyzip::decompress(istream, ostream),
        }
    }
}

#[derive(Debug)]
enum Error {
    Whyzip(whyzip::Error),
    Parser(cmdparse::Error),
    Io(io::Error),
    InvalidSuffix,
}

impl From<whyzip::Error> for Error {
    fn from(e: whyzip::Error) -> Self {
        Self::Whyzip(e)
    }
}

impl From<cmdparse::Error> for Error {
    fn from(e: cmdparse::Error) -> Self {
        Self::Parser(e)
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::Io(e)
    }
}

fn main() -> Result<(), Error> {
    let parser_config = cmdparse::Input::Command(cmdparse::InputCommandBuilder::new()
        .add_arg("decompress".to_string(), cmdparse::ArgumentBuilder::new(cmdparse::ArgumentType::Option { long: Some("decompress".to_string()), short: Some('d') })
            .build())
        .add_arg("keep".to_string(), cmdparse::ArgumentBuilder::new(cmdparse::ArgumentType::Option { long: Some("keep".to_string()), short: Some('k') })
            .build())
        .add_arg("suffix".to_string(), cmdparse::ArgumentBuilder::new(cmdparse::ArgumentType::Option { long: Some("suffix".to_string()), short: Some('S') })
            .value(cmdparse::InputValue::String)
            .default(cmdparse::OutputValue::String(".why".to_string()))
            .build())
        .add_arg("file".to_string(), cmdparse::ArgumentBuilder::new(cmdparse::ArgumentType::Positional(0))
            .repeating(cmdparse::ArgumentRepeat::Yes)
            .value(cmdparse::InputValue::String)
            .build())
        .build()
    );

    let args: Vec<String> = env::args().collect();
    let bin_name = args.get(0);
    let (_, parsed_command) = cmdparse::parse_args(&args[1..], &parser_config)?;

    let action = if parsed_command["decompress"].try_value().is_some() || (bin_name.is_some() && bin_name.unwrap().ends_with("whyunzip")) {
        Action::Decompress
    } else {
        Action::Compress
    };
    let suffix = parsed_command["suffix"].value()?.as_str()?;
    let keep = parsed_command["keep"].try_value().is_some();
    let file_paths = &parsed_command["file"].values;

    if !file_paths.is_empty() {
        // Compress files inplace
        for file_path in file_paths {
            let input_path = file_path.as_str()?;
            let output_path = match action {
                // Append/delete suffix
                Action::Compress => {
                    let mut path = input_path.to_string();
                    path.push_str(suffix);

                    path
                },
                Action::Decompress => {
                    if let Some(path) = input_path.strip_suffix(suffix) {
                        path.to_string()
                    } else {
                        return Err(Error::InvalidSuffix);
                    }
                },
            };

            let mut input_file = File::open(input_path)?;
            let mut output_file = File::create(output_path)?;

            action.execute_on(&mut input_file, &mut output_file)?;

            // Remove the original
            if !keep {
                fs::remove_file(input_path)?;
            }
        }
    } else {
        // Compress stdin and write it to stdout
        action.execute_on(&mut io::stdin(), &mut io::stdout())?;
    }

    Ok(())
}
