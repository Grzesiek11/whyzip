use std::io::{Read, Write, self};

const MAGIC: &[u8; 3] = b"why";

#[derive(Debug)]
pub enum Error {
    InvalidMagic,
    FileCorrupted,
    Io(io::Error),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

pub fn decompress<I: Read, O: Write>(istream: &mut I, ostream: &mut O) -> Result<(), Error> {
    let mut ostream_buffered = io::BufWriter::new(&mut *ostream);

    // Read header and confirm it has the whyzip magic
    let mut header = [0; 3];
    istream.read_exact(&mut header)?;
    if &header != MAGIC {
        return Err(Error::InvalidMagic);
    }

    // Read and write bytes
    let mut buf = [0; 2];
    loop {
        match istream.read(&mut buf)? {
            0 => break,
            n if n < buf.len() => return Err(Error::FileCorrupted),
            _ => (),
        }

        let ch = buf[0];
        let count_bytes = buf[1];

        let count = {
            let mut count_buf = [0; 16];
            let count_slice = &mut count_buf[16 - (count_bytes as usize)..];

            istream.read_exact(count_slice)?;
            u128::from_be_bytes(count_buf.try_into().unwrap())
        };

        for _ in 0..count + 1 {
            ostream_buffered.write_all(&[ch])?;
        }
    }

    Ok(())
}

pub fn compress<I: Read, O: Write>(istream: &mut I, ostream: &mut O) -> Result<(), Error> {
    let istream_buffered = io::BufReader::new(&mut *istream);

    fn write<O: Write>(byte: u8, count: u128, ostream: &mut O) -> Result<(), Error> {
        // Write byte
        ostream.write_all(&[byte])?;

        // Convert count to bytes
        let mut count_bytes: Vec<u8> = count.to_be_bytes().into();

        // Strip leading null bytes
        let mut last_null = 0;
        for (i, byte) in count_bytes.iter().enumerate() {
            if *byte == 0 {
                last_null = i;
            } else {
                break;
            }
        }
        count_bytes.drain(..last_null + 1);

        // Write count byte count
        ostream.write_all(&[count_bytes.len() as u8])?;
        // Write count
        ostream.write_all(&count_bytes)?;

        Ok(())
    }

    // Write magic
    ostream.write_all(MAGIC)?;

    // Count and write bytes
    let mut count: u128 = 0;
    let mut last_byte = None;
    for byte in istream_buffered.bytes() {
        let byte = byte?;

        if let Some(last_byte) = last_byte {
            if last_byte == byte {
                count += 1;
            } else {
                write(last_byte, count, ostream)?;
                count = 0;
            }
        }

        last_byte = Some(byte);
    }

    if let Some(last_byte) = last_byte {
        write(last_byte, count, ostream)?;
    }

    Ok(())
}
